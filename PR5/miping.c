// Practica 5, Alfageme Sainz Samuel

/* PRACTICA 5 - ARQUITECTURA DE REDES Y SERVICIOS
 *              - BASIC PING SERVICE -
 *
 * @version 1.1
 * @author SAMUEL ALFAGEME SAINZ <samuel.alfageme@alumnos.uva.es>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ip-icmp-ping.h" // Estructuras para la practica

#define VB 10
#define QT 11

#define VERBOSE verbosity == VB

int verbosity = QT;

void usage(char *name);
unsigned short int calculateChecksum(ECHORequest request);
void describe_response(ECHOResponse response);
void describe_DU(unsigned char code);

int main(int argc, char **argv){

  int socket_desc;
  struct sockaddr_in origin, destination;

  // ============ Validacion del input de usuario =============================
  if(argc != 2 &&  argc != 3)
      usage(argv[0]);

  if(argc == 2)
      verbosity = QT;
  else if(argv[2][0]=='-' && argv[2][1]=='v')
      verbosity = VB;
  else
      usage(argv[0]);

  // ================= Parametros de la conexion: =============================

  // Creacion del socket RAW para las comunicaciones ICMP:
  if( (socket_desc = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0)
  {
      perror("socket()");
      exit(EXIT_FAILURE);
  }

  // Direccion del origen del PING:
  bzero(&origin, sizeof(origin));
  origin.sin_family = AF_INET;
  origin.sin_port = 0;
  origin.sin_addr.s_addr = INADDR_ANY;

  // Direccion del destino del PING:
  bzero(&destination, sizeof(destination));
  destination.sin_family = AF_INET;
  destination.sin_port = 0;
  // Extraemos la IP pasada como argumento al programa
  if(inet_aton(argv[1], &destination.sin_addr) <= 0)
  {
      perror("inet_aton()");
      exit(EXIT_FAILURE);
  }

  // ============ Inicializacion de estructuras para el PING: =================

  // Inicializacion vacia de las estructuras para los mensajes ICMP
  ECHORequest  echoRequest  = {};
  ECHOResponse echoResponse = {};

  // Seteamos los campos de la cabecera ICMP
  echoRequest.icmpHeader.Type = 0x08;         // Echo Request (ping)
  echoRequest.icmpHeader.Code = 0x00;
  echoRequest.icmpHeader.Checksum = htons(0); // Seteamos el checksum a 0

  // El ID de la peticion sera el pid del proceso (en orden de red)
  echoRequest.ID = htons((unsigned short) getpid() & 0xffff);
  echoRequest.SeqNumber = htons(0);

  // Introducimos el payload en la peticion
  strncpy(echoRequest.payload, "Este es el payload", REQ_DATASIZE);

  // Calculamos el checksum del segmento a enviar y lo insertamos en el mismo
  echoRequest.icmpHeader.Checksum = calculateChecksum(echoRequest);

  if(VERBOSE){
    printf("-> Generando cabecera ICMP.\n");
    printf("-> Type: %1x.\n",echoRequest.icmpHeader.Type);
    printf("-> Code: %1x.\n",echoRequest.icmpHeader.Code);
    printf("-> Identifier (pid): %hu.\n",ntohs(echoRequest.ID));
    printf("-> Seq. number %hu.\n", echoRequest.SeqNumber);
    printf("-> Cadena a enviar: %s.\n",echoRequest.payload);
    printf("-> Checksum: 0x%04X.\n",ntohs(echoRequest.icmpHeader.Checksum));
    printf("-> Tamaño total del paquete ICMP: %lu\n",sizeof(echoRequest));
  }

  // =================== Intercambio de mensajes ICMP: ========================

  // Enviamos la peticion al destino del ping
  if(sendto(socket_desc, &echoRequest, sizeof(ECHORequest), 0,
      (struct sockaddr *) &destination, sizeof(destination)) < 0)
  {
      perror("sendto()");
      exit(EXIT_FAILURE);
  }

  printf("Paquete ICMP enviado a %s\n", inet_ntoa(destination.sin_addr));

  socklen_t len = sizeof destination;

  // Llamada bloqueante hasta que se reciba la respuesta al ping
  if(recvfrom(socket_desc, &echoResponse, sizeof(ECHOResponse), 0,
      (struct sockaddr *) &destination, &len) < 0)
  {
      perror("recvfrom()");
      exit(EXIT_FAILURE);
  }

  // Extraemos la direccion del emisor (que puede coincidir o no con el destino
  // de la peticion original) de la respuesta
  printf("Respuesta recibida desde %s\n",
      inet_ntoa(echoResponse.ipHeader.iaSrc));

  if (VERBOSE) {
    printf("-> Tamaño de la respuesta: %hi\n",
        ntohs(echoResponse.ipHeader.TotLen));
    printf("-> Cadena recibida: %s.\n",echoResponse.payload);
    printf("-> Identifier (pid): %hu.\n",ntohs(echoResponse.ID));
    printf("-> TTL: %d.\n",echoResponse.ipHeader.TTL);
  }

  // Describimos la respuesta de forma legible al usuario
  describe_response(echoResponse);

  return 0;
}

// Calculo del checksum de un paquete ECHORequest en una variable de 16bytes
unsigned short int calculateChecksum(ECHORequest request){
  int numShorts = sizeof(ECHORequest) / 2;
  unsigned short int *elemento = (unsigned short int*) &request;
  unsigned int checksum = 0;
  int i;
  for(i=0;i<numShorts;i++){
    checksum = checksum + (unsigned int) *elemento;
    elemento++;
  }
  checksum = (checksum >> 16) + (checksum & 0x0000ffff);
  checksum = (checksum >> 16) + (checksum & 0x0000ffff);
  return ~checksum;
}

// Descripcion de una respuesta en base a los campos Type y Code:
// Nota: solo se han contemplado respuestas ICMP (excepto *)
void describe_response(ECHOResponse response){
  printf("Descripcion de la respuesta: ");
  switch (response.icmpHeader.Type) {
    case 0x0:
      if(response.icmpHeader.Code == 0x0)
        printf("respuesta correcta ");
      break;
    case 0x3:
      printf("Destination Unreachable: ");
      describe_DU(response.icmpHeader.Code);
      break;
    case 0x5:
      printf("Redirect Message ");
      switch (response.icmpHeader.Code) {
        case 0x0:
          printf("Redirect Datagram for the Network ");
          break;
        case 0x1:
          printf("Redirect Datagram for the Host ");
          break;
        case 0x2:
          printf("Redirect Datagram for the TOS & network ");
          break;
        case 0x3:
          printf("Redirect Datagram for the TOS & host ");
          break;
      }
      break;
    // *En el caso de que enviemos el ping contra localhost, la respuesta sera
    // la peticion tal cual, con el Type 0x8
    case 0x8:
        printf("Echo Request ");
        break;
    case 0xB:
      printf("Time Exceeded ");
      switch (response.icmpHeader.Code) {
        case 0x0:
          printf("TTL expired in transit ");
          break;
        case 0x1:
          printf("Fragment reassembly time exceeded ");
          break;
        }
      break;
    case 0xC:
      printf("Parameter Problem: Bad IP header ");
      switch (response.icmpHeader.Code) {
        case 0x0:
          printf("Pointer indicates the error ");
          break;
        case 0x1:
          printf("Missing a required option ");
          break;
        case 0x2:
          printf("Bad lenght ");
          break;
      }
      break;
  }
  printf("(type %1x, code %1x).\n",
      response.icmpHeader.Type,response.icmpHeader.Code);
}

// Descripcion de una respuesta Destination Unreachable en base al Code asoc.
void describe_DU(unsigned char code){
  switch (code) {
    case 0x0:
      printf("Destination network unreachable ");
      break;
    case 0x1:
      printf("Destination host unreachable ");
      break;
    case 0x2:
      printf("Destination protocol unreachable ");
      break;
    case 0x3:
      printf("Destination port unreachable ");
      break;
    case 0x4:
      printf("Fragmentation required, and DF flag set ");
      break;
    case 0x5:
      printf("Source route failed ");
      break;
    case 0x6:
      printf("Destination network unknown ");
      break;
    case 0x7:
      printf("Destination host unknown ");
      break;
    case 0x8:
      printf("Source host isolated ");
      break;
    case 0x9:
      printf("Network administratively prohibited ");
      break;
    case 0xA:
      printf("Host administratively prohibited ");
      break;
    case 0xB:
      printf("Network unreachable for TOS ");
      break;
    case 0xC:
      printf("Host unreachable for TOS ");
      break;
    case 0xD:
      printf("Communication administratively prohibited ");
      break;
    case 0xE:
      printf("Host Precedence Violation ");
      break;
    case 0xF:
      printf("Precedence cutoff in effect ");
      break;
  }
}

void usage(char *name){
  fprintf(stderr, "%s <PING destination IP> [-v]\n", name);
  exit(EXIT_FAILURE);
}
