/* PRACTICA 4 - ARQUITECTURA DE REDES Y SERVICIOS
 *                 - TFTP CLIENT -
 *
 * @version 1.0
 * @author SAMUEL ALFAGEME SAINZ <samuel.alfageme@alumnos.uva.es>
 */

 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <strings.h>
 #include <errno.h>
 #include <netdb.h>
 #include <unistd.h>
 #include <sys/types.h>
 #include <sys/socket.h>
 #include <netinet/ip.h>
 #include <netinet/in.h>
 #include <arpa/inet.h>

// Valores que pueden tomar las flags del programa:
#define VERBS       10
#define QUIET   11
#define READ    100
#define WRITE   101

// TFTP package constants
#define PAYLOAD_MAX_LEN 512
#define PACKAGE_MAX_LEN 2 + 2 + PAYLOAD_MAX_LEN

// TFTP opcodes
#define RRQ 1 // Read Request
#define WRQ 2 // Write Request
#define DAT 3 // Data
#define ACK 4 // ACK
#define ERR 5 // Error

// TFTP errcode
#define ERR_NODEF 0 // No definido. Comprobar errstring
#define ERR_NOFOU 1 // Fichero no encontrado
#define ERR_ACCVI 2 // Violacion de acceso
#define ERR_NOSPA 3 // Espacio de almacenamiento lleno
#define ERR_ILEGA 4 // Operacion TFTP ilegal
#define ERR_UNKNO 5 // Id de transferencia desconocido
#define ERR_EXIST 6 // El fichero ya existe
#define ERR_USRUN 7 // Usuario desconocido

// TFTP implemented transfer modes:
#define OCTET "octet"

// Flags auxiliares para la funcion process_response:
#define KEEP 1 // Quedan paquetes por recibir, continuar mandando ACK
#define LAST 2 // Ultimo paquete recibido, mandar el ultimo ACK

void usage(char* program);
void request(unsigned char *req, int mode, char *fname);
void print_file(FILE *f, unsigned char *package);
void print_package(unsigned char *package, int reqsize);
int  process_response(unsigned char *response);
void gen_response(unsigned char *response, int type, int blockno,
        unsigned char *payload);
int  is_last(unsigned char *payload);
void handle_error(unsigned char *error);

int verbosity;

#define VERBOSE verbosity==VERBS

int main(int argc, char **argv)
{
    int mode;
    int tftp_port, socket_desc;
    struct sockaddr_in client, server;

    char *hostname = argv[1];
    char *fname = argv[3];

    // ============ Validacion del input de usuario =============================
    if(argc != 4 &&  argc != 5)
        usage(argv[0]);

    if(argv[2][0] == '-' )
    {
        if(argv[2][1] == 'w')
            mode = WRITE;
        else if(argv[2][1] == 'r')
            mode = READ;
        else
            usage(argv[0]);
    }
    else
        usage(argv[0]);

    if(argc == 4)
        verbosity = QUIET;
    else if(argv[4][0]=='-' && argv[4][1]=='v')
        verbosity = VERBS;
    else
        usage(argv[0]);

    // ================= Parametros de la conexion: =============================

    // Seleccion del puerto correspondiente al servicio TFTP de /etc/services
    struct servent *tftp_service;
    tftp_service = getservbyname("tftp","udp");
    tftp_port = tftp_service->s_port;

    // Creacion del socket UDP para la transmision
    if( (socket_desc = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    // Direccion del cliente
    bzero(&client, sizeof(client));
    client.sin_family = AF_INET;
    client.sin_port = 0;
    client.sin_addr.s_addr = INADDR_ANY;

    // Enlazado de la direccion que acabamos de settear con el socket
    if(bind(socket_desc, (struct sockaddr *) &client, sizeof(client)) < 0)
    {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Direccion del servidor TFTP
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = tftp_port;

    if(inet_aton(argv[1], &server.sin_addr) <= 0)
    {
        perror("inet_aton()");
        exit(EXIT_FAILURE);
    }

    // ================= Operativa del cliente TFTP: ============================

    // Comprobamos si se dispone de derechos de acceso al fichero a enviar
    if(mode == WRITE && access(fname, F_OK) == -1)
    {
        fprintf(stderr, "El fichero %s de su sistema no es accesible\n", fname);
        exit(EXIT_FAILURE);
    }

    if(VERBOSE){
        char *modo = (char *)malloc(9);
        if(mode==WRITE) modo = "escritura";
        else modo = "lectura";
        printf("Enviada solicitud de %s de fichero %s a servidor tftp en %s\n",
                modo,fname,hostname);
    }

    // Primer dialogo con el servidor: primera peticion del protocolo:
    int size = 2 + strlen(fname) + 1 + strlen(OCTET) + 1;
    unsigned char *peticion = (unsigned char *)malloc(size);

    // Generamos y enviamos dicha peticion que abre el dialogo con el servidor:
    request(peticion,mode,fname);

    if(sendto(socket_desc, peticion, size, 0,
            (struct sockaddr *) &server, sizeof(server)) < 0)
    {
        perror("sendto()");
        exit(EXIT_FAILURE);
    }

    free(peticion);

    // La respuesta del servidor se almacenara en data
    unsigned char data[PACKAGE_MAX_LEN];
    bzero(data,PACKAGE_MAX_LEN);
    socklen_t len = sizeof server;

    if(recvfrom(socket_desc, data, sizeof(data), 0,
            (struct sockaddr *) &server, &len) < 0)
    {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }

    // Si la respuesta consiste en un paquete de error, lo gestionamos:
    if(process_response(data) == -1){
        handle_error(data);
        exit(EXIT_FAILURE);
    }

    // En caso de que no sea un error, seleccionamos uno de los modos de operacion
    switch(mode){
        case(READ):
            {
            FILE *f;

            if((f = fopen(fname,"w")) == NULL)
            {
                perror("fopen()");
                exit(EXIT_FAILURE);
            }

            // La primera respuesta del servidor contiene el primer payload a
            // imprimir en el fichero, suponemos que las respuestas estan ordenadas
            print_file(f,data);

            while(process_response(data) == KEEP || process_response(data) == LAST){

                unsigned char *ack = (unsigned char *)malloc(4);

                int blockn;
                memcpy(&blockn,&data[2],2);
                blockn = htons(blockn);

                if(VERBOSE){
                    printf("Recibido bloque del servidor tftp\n");
                    if(blockn==1)
                        printf("Es el primer bloque (numero de bloque 1)\n");
                    else
                        printf("Es el bloque con codigo %d\n",blockn);
                }

                // Generamos el ACK de respuesta y lo enviamos al servidor
                gen_response(ack,ACK,blockn,NULL);

                if(VERBOSE)
                    printf("Enviamos el ACK del bloque %d.\n",blockn);

                if(sendto(socket_desc, ack, 4, 0, (struct sockaddr *) &server,
                        sizeof(server)) < 0)
                {
                    perror("sendto()");
                    exit(EXIT_FAILURE);
                }

                if(process_response(data)==LAST){
                    if(VERBOSE)
                        printf("El bloque %d era el ultimo: cerramos el fichero.\n",blockn);
                    // No esperamos recibir mas paquetes del servidor, salimos del bucle:
                    break;
                }

                // Si no era la ultima respuesta, tenemos que continuar recibiendo
                // paquetes de datos del servidor:
                bzero(data,PACKAGE_MAX_LEN);

                if(recvfrom(socket_desc, data, sizeof(data), 0,
                        (struct sockaddr *) &server, &len) < 0)
                {
                    perror("recvfrom()");
                    exit(EXIT_FAILURE);
                }
                // Imprimimos en el fichero el payload de la respuesta recibida
                print_file(f,data);
            }
            fclose(f);
            break;
            }

        case(WRITE):
            {
            FILE *fp;

            if((fp = fopen(fname,"rb")) == NULL)
            {
                perror("fopen()");
                exit(EXIT_FAILURE);
            }

            // Determinamos la longitud y el numero de paquetes del fichero a enviar
            long fsize;
            fseek(fp,0L,SEEK_END);
            fsize = ftell(fp);
            rewind(fp);
            long nopackages = (fsize / PAYLOAD_MAX_LEN)+1;

            // Declaracion de los paquetes que se intercambian en el modo escritura
            unsigned char *datap         = (unsigned char *)malloc(PACKAGE_MAX_LEN);
            unsigned char *read_data     = (unsigned char *)malloc(PAYLOAD_MAX_LEN);
            unsigned char *ack           = (unsigned char *)malloc(4);

            // El primer paquete recibido sera un ACK, utilizamos el alias:
            ack = data;

            // Bucle de envio del programa: las variables real y expected contienen
            // los bloques real (recibido en el ACK del servidor) y esperado (supon-
            // iendo una transmision ordenada de paquetes)
            int real,expected;
            for(expected = 0, real = process_response(ack); real<nopackages ;
                    expected++,real=process_response(ack)){

                if (VERBOSE)
                    printf("Recibido el ACK numero %d del servidor tftp.\n",real);

                // Siempre que las variables anteriores coincidan transmitimos
                if(real==expected){
                    bzero(datap,PACKAGE_MAX_LEN);
                    bzero(read_data,PAYLOAD_MAX_LEN);
                    fread(read_data,1,PAYLOAD_MAX_LEN,fp);

                    // Generamos la respuesta al ACK del servidor, si respondio con un
                    // ACK x, mandaremos el paquete x+1 con los datos leidos del fichero
                    gen_response(datap,DAT,real+1,read_data);

                    if (VERBOSE) {
                        printf("Enviamos el paquete de datos numero %d.\n",real+1);
                    }

                    if(sendto(socket_desc, datap, PACKAGE_MAX_LEN, 0,
                            (struct sockaddr *) &server, sizeof(server)) < 0)
                    {
                        perror("sendto()");
                        exit(EXIT_FAILURE);
                    }

                    bzero(ack,4);

                    if(recvfrom(socket_desc, ack, 4, 0,
                            (struct sockaddr *) &server, &len) < 0)
                    {
                        perror("recvfrom()");
                        exit(EXIT_FAILURE);
                    }

                } else {
                    fprintf(stderr, "Ocurrio un error de transmision, se esperaba el "
                        "paquete %d y se recibio el %d\n",expected,real);
                    exit(EXIT_FAILURE);
                }
            }
            if (VERBOSE)
                printf("Recibido el ACK numero %d del servidor tftp. Era el ultimo: "
                    "el fichero se envio por completo.\n",real);
            fclose(fp);
            break;
        }
    }
    close(socket_desc);
    return 0;
}

void request(unsigned char *req, int mode, char *fname){
    req[0] = 0x00;
    req[1] = mode == WRITE ? WRQ : RRQ;
    memcpy(&req[2],fname,strlen(fname)+1);
    memcpy(&req[2+strlen(fname)+1],(unsigned char *)OCTET,
        strlen(OCTET)+1);
}

void gen_response(unsigned char *response, int type, int blockno,
        unsigned char *payload){
    int blockno_no = htons(blockno);
    memcpy(&response[2],(unsigned char*)&blockno_no,2);
    switch(type){
        case DAT:
            response[0] = 0x00 ; response[1] = 0x03;
            memcpy(&response[4],payload,strlen((char *) payload));
            break;
        case ACK:
            response[0] = 0x00 ; response[1] = 0x04;
            break;
    }
}

void print_package(unsigned char *package,int reqsize){
    int i;
    for(i=0;i<reqsize;i++)
        printf("%02x ",package[i]);
    printf("\n");
}

void print_file(FILE *f, unsigned char *package){
    unsigned char *payload = (unsigned char *)&package[4];
    fprintf(f, "%s", (char *)payload);
}

int process_response(unsigned char *response){
    unsigned char opcode = response[1];
    int blockn;
    memcpy(&blockn,&response[2],2);
    blockn = htons(blockn);
    switch(opcode){
        case DAT:
            return is_last((unsigned char *)&response[4]);
        case ACK:
            return blockn;
        case ERR:
            return -1;
    }
    return -1;
}

int is_last(unsigned char *payload){
    int i = 0;
    while(payload[i]!=0x00)
        i++;
    return i == PAYLOAD_MAX_LEN ?  KEEP : LAST;
}

void handle_error(unsigned char *error){
	int errcode;
	memcpy(&errcode,&error[2],2);
	char *errstring =  (char *)&error[4];
	fprintf(stderr,"Error: ");
		switch (errcode) {
			case ERR_NODEF:
				fprintf(stderr,"%s\n",errstring);
				break;
			case ERR_NOFOU:
				fprintf(stderr,"Fichero no encontrado\n");
				break;
			case ERR_ACCVI:
				fprintf(stderr,"Violacion de acceso\n");
				break;
			case ERR_NOSPA:
				fprintf(stderr,"Espacio de almacenamiento lleno\n");
				break;
			case ERR_ILEGA:
				fprintf(stderr,"Operacion TFTP ilegal\n");
				break;
			case ERR_UNKNO:
				fprintf(stderr,"Identificador de transferencia desconocido\n");
				break;
			case ERR_EXIST:
				fprintf(stderr,"El fichero ya existe\n");
				break;
			case ERR_USRUN:
				fprintf(stderr,"Usuario desconocido\n");
				break;
		}
}

void usage(char *name){
    fprintf(stderr, "%s <TFTP server IP> {-r|-w} <file> [-v]\n", name);
    exit(EXIT_FAILURE);
}
