/* PRACTICA 3 - ARQUITECTURA DE REDES Y SERVICIOS
 * 	   - P1 - DAYTIME TCP CLIENT -
 *
 * @version 1.0
 * @author SAMUEL ALFAGEME SAINZ (samuel.alfageme@alumnos.uva.es)
 */

#include "arsutils.h"


int main(int argc, char **argv)
{
	int daytime_port, socket_desc;
	struct sockaddr_in client, server;

	if (argc != 2 &&  argc != 4)
	{
		fprintf(stderr, "%s <Daytime server IP> [-p <server port>]\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	if(argc == 2)
	{
		struct servent *daytime_service;
	        daytime_service = getservbyname("daytime","tcp");
		daytime_port = daytime_service->s_port;
	}
	else if(argv[2][0]=='-' && argv[2][1]=='p')
	{
		int hbo_port;
		sscanf(argv[3], "%d", &hbo_port);
		daytime_port = htons(hbo_port);
	}
	else
	{
		fprintf(stderr, "%s <Daytime server IP> [-p <server port>]\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	// Creacion del socket del cliente
	if( (socket_desc = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("socket()");
		exit(EXIT_FAILURE);
	}
	
	// Inicializacion de la direccion del cliente para ligarla al socket
	bzero(&client, sizeof(client));
	client.sin_family = AF_INET;		// Familia IPv4
	client.sin_port = 0;			// Cualquier puerto disponible en el sistema
	client.sin_addr.s_addr = INADDR_ANY;	// La direccion de la interfaz de red

	// Enlazado del socket a la direccion recien creada
	if(bind(socket_desc, (struct sockaddr *) &client, sizeof(client)) < 0)
	{
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	// Creacion de la estructura sockaddr_in del servidor:
	bzero(&server, sizeof(server));
	server.sin_family = AF_INET;	
	server.sin_port = daytime_port;		// El puerto setteado por argumento o por defecto
	// Traduccion de la IP pasada como argumento a network byte order y asignacion al campo de la direccion de la estructura sockaddr_in del servidor
	if(inet_aton(argv[1], &server.sin_addr) <= 0)
	{
		perror("inet_aton()");
		exit(EXIT_FAILURE);
	}

	// Conexion del socket a la direccion del servidor
	if(connect(socket_desc, (struct sockaddr *) &server, sizeof(server)) < 0)
	{
		perror("connect()");
		exit(EXIT_FAILURE);
	}

	// Tras realizar la conexion, llamada bloqueante a recv hasta que se reciba el primer mensaje del servidor
	char respuesta[100];
	if(recv(socket_desc, respuesta, sizeof(respuesta), 0) < 0)
	{
		perror("recvfrom()");
		exit(EXIT_FAILURE);
	}

	// Envio del mensaje FIN sobre el socket para cerrar la conexion
	shutdown(socket_desc,SHUT_RDWR);
	// Impresion del mensaje recibido
	printf("%s",respuesta);

	return 0;
}
