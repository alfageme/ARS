/* Arquitectura de Redes y Servicios
 * Biblioteca de utilidades para servidores
 *
 * @author Samuel Alfageme (samuel.alfageme@alumnos.uva.es)
 * @version 1.0
 */

#include <signal.h>

#define MAX_COLA 30

extern int socket_desc;

// Generacion del timestamp con el formato <hostname>: <timestamp> en el puntero *ts
int generateTimestamp(char *ts, size_t ts_len)
{
	char hostname[ts_len/2];

	if(gethostname(hostname, ts_len/2) < 0)
	{
		// Settear el codigo de errno en esta funcion
		perror("gethostname()");
		return(-1);
	}

	strcpy(ts,hostname);
	strcat(ts,": ");

	char date[ts_len/2];
	FILE *fich;

	system("date > /tmp/tt.txt");
	fich = fopen("/tmp/tt.txt","r");

	if(fgets(date,ts_len/2,fich)==NULL)
	{
		perror("fgets()");
		return(-1);
	}

	strcat(ts,date);

	return 0;
}

// Manejador de las signal que se produzcan en el servidor
void signal_handler(int sig)
{
	// Se ignora la signal original, pues aqui se esta gestionando
	signal(sig, SIG_IGN);
	// Se libera el socket de escucha y se finaliza la ejecucion del programa
	shutdown(socket_desc,SHUT_RDWR);
	exit(EXIT_SUCCESS);
}
