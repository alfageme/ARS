/* PRACTICA 3 - ARQUITECTURA DE REDES Y SERVICIOS
 *    - P2 - CONCURRENT TCP DAYTIME SERVER -
 *
 * @version 1.0
 * @author SAMUEL ALFAGEME SAINZ (samuel.alfageme@alumnos.uva.es)
 */

#include "arsutils.h"
#include "serverutils.h"

int socket_desc;

int main(int argc, char **argv)
{
	int daytime_port;
	int socket_aux;
	struct sockaddr_in host, client;

	if (argc != 1 &&  argc != 3)
	{
		fprintf(stderr, "%s [-p <server port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if(argc == 1)
	{
		// No se ha proporcionado puerto como argumento. Se extrae de /etc/services
		struct servent *daytime_service;
	        daytime_service = getservbyname("daytime","tcp");
		daytime_port = daytime_service->s_port;
	}
	else if(argv[1][0]=='-' && argv[1][1]=='p')
	{
		// Conversion del puerto pasado al programa a host-byte-order
		int hbo_port;
		sscanf(argv[2], "%d", &hbo_port);
		daytime_port = htons(hbo_port);
	}
	else
	{
		fprintf(stderr, "%s [-p <server port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// Creacion del socket tipo TCP que servira como punto de escucha a clientes
	if( (socket_desc = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	// Inicializamos la direccion que tendra el socket:
	bzero(&host, sizeof(host));
	host.sin_family = AF_INET;		// Familia IPv4
	host.sin_port = daytime_port;		// Puerto especificado o extraido de /etc/services
	host.sin_addr.s_addr = INADDR_ANY; 	// Direccion de cualquiera de las interfaces de red

	// Enlazamos el soket con la direccion que acabamos de crear
	if(bind(socket_desc, (struct sockaddr *) &host, sizeof(host)) < 0)
	{
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	// Ponemos el socket en modo escucha para recibir conexiones externas
	if(listen(socket_desc, MAX_COLA) < 0)
	{
		perror("listen()");
		exit(EXIT_FAILURE);
	}

	// Bucle de servicio; el proceso se ejecuta hasta que se reciba un SIGINT
	signal(SIGINT, signal_handler);
	while(1)
	{
		// Inicializacion de la estructura que representa la dir. del socket auxiliar
		bzero(&client, sizeof(client));
		socklen_t len = sizeof client;

		// Llamada bloqueante que acepta la conexion de un cliente sobre este socket auxiliar
		if( (socket_aux = accept(socket_desc, (struct sockaddr *) &client, &len)) < 0)
		{
			perror("accept()");
			exit(EXIT_FAILURE);
		}

		// Soporte a la concurrencia del servidor:
		pid_t pid;
		if( (pid = fork()) == -1)
		{
			perror("fork()");
			exit(EXIT_FAILURE);
		}
		// Codigo del proceso hijo:
		else if(pid == 0)
		{
			// Se disminuye el numero de referencias abiertas al socket de escucha
			close(socket_desc);

			// Generacion de la respuesta
			char timestamp[128];
			if(generateTimestamp(timestamp, sizeof timestamp) < 0)
			{
				perror("generateTimestamp()");
				exit(EXIT_FAILURE);
			}

			// Envio del timestamp por el socket de conexion con el cliente
			if(send(socket_aux, timestamp, sizeof(timestamp), 0) < 0)
			{
				perror("send()");
				exit(EXIT_FAILURE);
			}

			// Se cierra el socket TCP que se mantenia con el cliente
			recv(socket_aux,NULL,0,0);
			shutdown(socket_aux, SHUT_RDWR);
			close(socket_aux);
			// El subproceso termina
			exit(EXIT_SUCCESS);
		}
		// Cierre del descriptor del socket auxiliar en el padre para no mantener la referencia tras pasarsela al subproceso, que se encargara de cerrarlo definitivamente
		close(socket_aux);
	}

	return 0;
}
