/* PRACTICA 2 - ARQUITECTURA DE REDES Y SERVICIOS
 * 	- P1 - SIMPLE UDP DAYTIME CLIENT -
 *
 * @version 1.0
 * @author SAMUEL ALFAGEME SAINZ (samuel.alfageme@alumnos.uva.es)
 */

#include "arsutils.h"

int main(int argc, char **argv)
{
	int daytime_port, socket_desc;
	struct sockaddr_in client, server;

	if (argc != 2 &&  argc != 4)
	{
		fprintf(stderr, "%s <Daytime server IP> [-p <server port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if(argc == 2)
	{
		struct servent *daytime_service;
	        daytime_service = getservbyname("daytime","udp");
		daytime_port = daytime_service->s_port;
		//printf("The port for the %s service is: %d\n",daytime_service->s_name,ntohs(daytime_port));
	}else if(argv[2][0]=='-' && argv[2][1]=='p'){
		int hbo_port;
		sscanf(argv[3], "%d", &hbo_port);
		daytime_port = htons(hbo_port);
		//printf("The input port in network byte order is: %d\n",daytime_port);
	}else{
		fprintf(stderr, "%s <Daytime server IP> [-p <server port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if( (socket_desc = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("socket()");
		exit(EXIT_FAILURE);
	}
	
	bzero(&client, sizeof(client));

	client.sin_family = AF_INET;
	client.sin_port = 0;
	client.sin_addr.s_addr = INADDR_ANY;


	if(bind(socket_desc, (struct sockaddr *) &client, sizeof(client)) < 0)
	{
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	bzero(&server, sizeof(server));

	server.sin_family = AF_INET;
	server.sin_port = daytime_port;

	// Traduccion de la IP pasada como argumento a network byte order
	if(inet_aton(argv[1], &server.sin_addr) <= 0)
	{
		perror("inet_aton()");
		exit(EXIT_FAILURE);
	}

	char *peticion = "You know what time is it?";

	if(sendto(socket_desc, peticion, sizeof(peticion), 0, (struct sockaddr *) &server, sizeof(server)) < 0)
	{
		perror("sendto()");
		exit(EXIT_FAILURE);
	}

	char respuesta[100];
	socklen_t len = sizeof server;

	if(recvfrom(socket_desc, respuesta, sizeof(respuesta), 0, (struct sockaddr *) &server, &len) < 0)
	{
		perror("recvfrom()");
		exit(EXIT_FAILURE);
	}

	//close(socket_desc);

	printf("%s",respuesta);

	return 0;
}
