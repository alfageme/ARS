/* Arquitectura de Redes y Servicios
 * Biblioteca de funciones para las practicas
 *
 * @author Samuel Alfageme (samuel.alfageme@alumnos.uva.es)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
