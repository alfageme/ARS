/* PRACTICA 2 - ARQUITECTURA DE REDES Y SERVICIOS
 *     - P2 - ITERATIVE UDP DAYTIME SERVER -
 *
 * @version 1.0
 * @author SAMUEL ALFAGEME SAINZ (samuel.alfageme@alumnos.uva.es)
 */

#include "arsutils.h"

int generateTimestamp(char *ts, size_t ts_len);

int main(int argc, char **argv)
{
	int daytime_port, socket_desc;
	struct sockaddr_in host, client;
	
	if (argc != 1 &&  argc != 3)
	{
		fprintf(stderr, "%s [-p <server port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if(argc == 1)
	{
		struct servent *daytime_service;
	        daytime_service = getservbyname("daytime","udp");
		daytime_port = daytime_service->s_port;
	}else if(argv[1][0]=='-' && argv[1][1]=='p'){
		int hbo_port;
		sscanf(argv[2], "%d", &hbo_port);
		daytime_port = htons(hbo_port);
	}else{
		fprintf(stderr, "%s [-p <server port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}


	if( (socket_desc = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("socket()");
		exit(EXIT_FAILURE);
	}
	
	bzero(&host, sizeof(host));

	host.sin_family = AF_INET;
	host.sin_port = daytime_port;
	host.sin_addr.s_addr = INADDR_ANY;


	if(bind(socket_desc, (struct sockaddr *) &host, sizeof(host)) < 0)
	{
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	while(1)
	{
		bzero(&client, sizeof(client));
		client.sin_family = AF_INET;
		client.sin_port = daytime_port;
		client.sin_addr.s_addr = INADDR_ANY;

		char peticion[100];
		socklen_t len = sizeof client;

		if(recvfrom(socket_desc, peticion, sizeof(peticion), 0, (struct sockaddr*) &client, &len)<0)
		{
			perror("recvfrom()");
			exit(EXIT_FAILURE);
		}


		char timestamp[128];

		if(generateTimestamp(timestamp, sizeof timestamp) < 0)
		{
			perror("generateTimestamp()");
			exit(EXIT_FAILURE);	
		}
		
		if(sendto(socket_desc, timestamp, sizeof(timestamp), 0, (struct sockaddr*) &client, len)<0)
		{
			perror("sendto()");
			exit(EXIT_FAILURE);
		}
	}

	return 0;
}

int generateTimestamp(char *ts, size_t ts_len)
{
	char hostname[ts_len/2];

	if(gethostname(hostname, ts_len/2) < 0)
	{
		// Settear el codigo de errno en esta funcion
		perror("gethostname()");
		return(-1);
	}

	strcpy(ts,hostname);
	strcat(ts,": ");

	char date[ts_len/2];
	FILE *fich;

	system("date > /tmp/tt.txt");
	fich = fopen("/tmp/tt.txt","r");
	
	if(fgets(date,ts_len/2,fich)==NULL)
	{
		perror("fgets()");
		return(-1);
	}

	strcat(ts,date);

	return 0;
}
